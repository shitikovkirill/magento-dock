DOCKER_DEV=docker-compose -f docker-compose.yml -f docker-compose.dev.yml
DOCKER_PROD=docker-compose -f docker-compose.yml -f docker-compose.prod.yml
DOCKER_ADMIN=docker-compose -f docker-compose.admin.yml

ENV=DEV
CONT=fpm
TAG=latest
DH_ID?=$(error Enter doker ID)
DH_INAME?=$(error  Enter doker image name)
DF_PATH?=$(error  Path to docker file)

install:
	$(DOCKER_DEV) run --rm cli magento-installer

up:
	$(DOCKER_$(ENV)) up

pull:
	$(DOCKER_$(ENV)) pull

stop:
	$(DOCKER_$(ENV)) stop

down:
	$(DOCKER_$(ENV)) down

down_full:
	$(DOCKER_$(ENV)) down -v

command:
	$(DOCKER_$(ENV)) run --rm cli magento-command $(CMD)

fix_perm:
	$(DOCKER_$(ENV)) run --rm cli bash -c "pwd && ls generated/ &&  chmod -R 777 generated/ && ls var/cache/ && chmod -R 777 var/cache/ && chmod -R 777 var/log/*"

bash:
	$(DOCKER_$(ENV)) run --rm $(CONT) bash

exec:
	$(DOCKER_$(ENV)) exec $(CONT) bash

clear: fix_perm clear_static clear_cache clear_generated;

renew_env:
	cp environment/composer.env.sample environment/composer.env
	cp environment/global.env.sample environment/global.env
	cp environment/magento.env.sample environment/magento.env

restore_bacup:
	$(DOCKER_DEV) exec db bash -c "zcat backup.sql.gz | mysql -u 'root' -p magento2"

clear_static:
	$(DOCKER_DEV) exec fpm bash -c "rm -rf pub/static/*"

clear_cache:
	$(DOCKER_DEV) exec fpm bash -c "rm -rf var/cache/*"

clear_generated:
	$(DOCKER_DEV) exec fpm bash -c "rm -rf generated/*"

docker_build:
	docker build -t $(DH_ID)/$(DH_INAME):$(TAG) $(DF_PATH)
	docker push $(DH_ID)/$(DH_INAME):$(TAG)
